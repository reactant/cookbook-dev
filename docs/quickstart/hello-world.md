# Quickstart: HelloWorld!
Yes, HelloWorld again.
## Simple Helloworld

We will use Gradle as build tools and Kotlin DSL as script file in this guide,
in case you use our CLI tool to build the project, you are not required to download Gradle.

!!! info ""
    Using Maven also fine, but no snippet of it in this tutorial will be provided.

### Reactant CLI

Luckily that we have a plugin project template and a CLI tool that can help us generate a blank project quickly, neat!

[<button class="waves-effect waves-light btn-small cyan">Get latest CLI</button>](https://gitlab.com/reactant/reactantcli/-/jobs/artifacts/master/raw/build/distributions/reactant-cli.zip?job=build)

Download the above zip, unzip it into a folder, and add that folder into `PATH` in environment variables (Just like what you do after downloaded JDK)



!!! info ""
    If you would like to create the project yourself, check [Manually Project Creation](../manually-project-creation.md).

### Create Project

Execute the following command in your terminal, and follow the instructions.

```bash
cd <your_work_directory>
reactant new HelloWorld
```

The CLI will ask the group name, in this quickstart, we will use `com.example` as our group name.

And obviously, we won't distribute our HelloWorld plugins to other.

After the command executed, a folder with HelloWorld should be created. Structure as following:

```
HelloWorld/
 ├ .gradle/
 ├ gradle/
 ├ src/main/
 │ ├ kotlin/
 │ │ └ com/example/helloworld/
 │ │   └ HelloWorld.kt
 │ └ resources/
 │   └ plugin.yml
 └ ... 
```

And you will see your HelloWorld main class should be created with a default logger like as below:

!!! note "src/main/kotlin/com/example/helloworld/HelloWorld.kt"
    ~~~~kotlin
    @ReactantPlugin(["com.example.helloworld"])
    class HelloWorld : JavaPlugin(){
        companion object {
            @JvmStatic
            val logger: Logger = LogManager.getLogger("HelloWorld")
        }
    }
    ~~~~

!!! tip "About @ReactantPlugin"
    [@ReactantPlugin](../core/@reactant-plugin.md) Annotation is used to tell Reactant where to find your Components, and it will search recursively.
    Components out of the package will not be loaded.
    
    Remind that do not try to include the packages that are not owned by your plugin. Or it may break other Reactant plugins if you shaded their classes.

### Create a HelloWorld Component


We will not use our main class to log "HelloWorld!", 
instead we will create a [Component](../core/reactant-object.md) with [LifeCycleHook](../core/life-cycle-hook.md).

Now create the HelloWorldLogger class.

!!! note "src/main/kotlin/com/example/helloworld/HelloWorldLogger.kt"
    ~~~~kotlin
    @Component
    class HelloWorldLogger : LifeCycleHook{
        override fun onEnable(){
            HelloWorld.log.info("HelloWorld!")
        }
    }
    ~~~~
    
Congratulations, you have already done the Hello World plugin now. :)

Run command `./gradlew build` in folder "MyFirstPlugin" and find your plugin jar inside `build/libs`,
you should be able to see "HelloWorld!" after server loaded.

!!! question "Wait, shouldn't we construct the HelloWorldLogger in somewhere?"
    For all Component, you should never construct them yourself, Reactant will manage the instance of each Component, 
    and create them using the public parameterless constructor.
    In addition, `onEnable()` will be called when your Component initialize if it have implements LifeCycleHook.
