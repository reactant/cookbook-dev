#Quickstart: Event handling - Spiky Player
In this tutorial, We will add a feature into our HelloWorld Plugin,
 damage the damager when player attack by entities.
It assumed that you have already finish the steps on [HelloWorld](hello-world.md).


## Inject EventService
If you have used Spigot Event API before, you may imagine there have something like `Bukkit.getServer().getPluginManager().registerEvents()`.

But in reactant, we suggested use the EventService we provided, which allow you to handle the event with [ReactiveX](http://reactivex.io/).

To use `EventService`, you have to inject it as below:

!!! note "src/main/kotlin/com/example/helloworld/SpikyPlayerEventListener.kt"
    ~~~~kotlin hl_lines="3"
    
    @Component
    class SpikyPlayerEventListener(
        private val eventService: EventService
    ) : LifeCycleHook 
    ~~~~

After you declare a parameter in the primary constructor in a @Component class, 
Reactant will search the provider of the type, in this case it will search for a provider of `EventService`, 
and inject it as a primary constructor parameter when constructing.

Alternatively, you can use [@Inject](../core/@inject.md)  to inject an injectables as variable instead of using primary constructor,
but it is not suggested.
```kotlin
    @Inject private lateinit var eventService: EventService
```
    
!!! warning
    If you use variable injection, You should not use that variable in constructor, 
    since it is not yet injected when constructing,
    it will only available on or after LifeCycleHook `onEnable()` called.

## Listening the event

To listen the events in `onEnable()`, firstly we have to register the Component into EventService.
method `register(registrable: Registrable)` is available for all Component implemented LifeCycleHook.

!!! note "src/main/kotlin/com/example/helloworld/SpikyPlayerEventListener.kt"
    ~~~~kotlin hl_lines="8 9"
    
    @Component
    class SpikyPlayerEventListener(
        private val eventService: EventService
    ) : LifeCycleHook {
        private val logger = LogManager.getLogger(javaClass)
    
        override fun onEnable() {
            register(eventService) {
            } 
        }
    }
    ~~~~

Then get the event observable of the `EntityDamageByEntityEvent`, and subscribe it inside the event register block.

```kotlin
eventService.registerBy(this) {
    EntityDamageByEntityEvent::class.observable()
            .subscribe { logger.info("${it.damager.name} damaged ${it.entity}") }
}
```

Now you should able to see the logging message when a entity was damaged by an entity.

## Event Filtering

But this is not what we want, it should be only happened when the entity is player and the damager is a living entity.

Instead of using a `if` block, we can elegantly add a filter before `subscribe()`.

```kotlin hl_lines="2"
EntityDamageByEntityEvent::class.observable()
        .filter { it.entity is Player && it.damager is LivingEntity }
        .subscribe { logger.info("${it.damager.name} damaged ${it.entity}") }
```

## Event observable block
If you are going to listen an event multiple times with different filter,
there have an alternative way to listen the events:

```kotlin
// Normal event priority
EntityDamageByEntityEvent::class {
    filter { it.entity is Player }
            .subscribe { logger.info("Event condition 1") }
        
    filter { it.entity !is Player }
            .subscribe { logger.info("Event condition 2") }
}

// Specified event priority
EntityDamageByEntityEvent::class priority EventPriority.HIGH  {
    /* ... */ 
}

```
