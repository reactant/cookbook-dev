![Reactant logo](img/reactant_logo_banner.png)
#Introduction
## What is reactant?

Reactant is a spigot plugin framework for developer to build their plugin faster and correctly.
Reactant core provided a better way for interaction between "services" and plugins by using dependency injection.
In addition, Reactant will provide tools that useful for developer to develop the spigot plugins in a elegant way.

## How should I start?

In this cookbook, we will use [Kotlin](https://kotlinlang.org/docs/reference/) as the code example language.
If you come from Java, ensure you have the basic knowledge of Kotlin, although they are similar.

!!! warning "Can I use Java?"
    We are not suggested you use Reactant with Java,
    you may get stuck on some problem such as [inline functions](https://kotlinlang.org/docs/reference/inline-functions.html) and [extension functions](https://kotlinlang.org/docs/reference/extensions.html).
    In addition, you won't be available to enjoy Kotlin's elegant DSL.
    
    TL;DR. Yes but use at your own risk. :)
Let's start our adventure when you are ready.

[<button class="waves-effect waves-light btn-small cyan">Take me to quickstart</button>](quickstart/hello-world.md)



## Contribute

If you are interest on it, please help us improve.

[<button class="waves-effect waves-light btn-small cyan">Gitlab</button>](https://gitlab.com/reactant)
