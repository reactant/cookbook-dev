#Location util

##Operators

|Operator|Example|Description|
|:------:|-------|-----------|
| +  | `loc3 = loc1 + loc2`     | Equal to `loc3 = loc1.clone().add(loc2)`        |
| -  | `loc3 = loc1 - loc2`     | Equal to `loc3 = loc1.clone().subtract(loc2)`   |
| *  | `loc2 = loc1 * num`      | Equal to `loc2 = loc1.clone().multiply(num)`    |
| /  | `loc2 = loc1 / num`      | Equal to `loc2 = loc1.clone().multiply(1/num)`  |

##Create function
###With x, y, z, yaw, pitch
####Definition
```kotlin
fun locationOf(
        world: World | String | UUID,
        x: Double, y: Double, z: Double, yaw: Float = 0F, pitch: Float = 0F
): Location
```
####Example
```kotlin
val loc = locationOf("world", 100.0, 200.0, 300.0)
```

###With vector
####Definition
```kotlin
fun locationOf(
        world: World | String | UUID,
        vector: Vector
): Location
```

####Example
```kotlin
val vec: Vector = // ...
val loc = locationOf("world", vec)
```

##Form area
See [Area](../area/index.md)