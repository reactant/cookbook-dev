#Area
Sometimes we have to find out the entities that inside a specified area, 
or check is whether the location inside the area, 
Reactant's area utils can help you create the area, combine the area, 
and do the above operation using the created area.

##Functions

!!! info "World checking"
    All checking functions of area will ignore locations' `world` property.
    Use [WorldArea](#worldarea) if world checking is needed.

!!! tip "Infix operator"
    `fun contains(sth)` is an operator and it is available to access with infix style like `val inside = loc in area`.
    
|Operator|Function definition|Description            |
|:------:|-------------------|-----------------------|
|   in   |`fun contains(loc: Location): Boolean`     |Return true if the location is in area|
|   in   |`fun contains(vec: Vector): Boolean`       |Return true if the vector is in area|
|   in   |`fun contains(entity: Entity): Boolean`    |Return true if the entity is in area|
|   in   |`fun contains(block: Block): Boolean`      |Return true if the block is in area|
|        |`fun move(vector: Vector): Area`           |Move the area|
|   +    |`fun plus(vector: Vector): Area`           |Equal to `area.clone().also{ move(vector) }`|
|        |`fun toWorldArea(world: World): WorldArea` |Convert Area to WorldArea|

##Create an area
```kotlin
val loc1 = Vector(0.0, 0.0, 0.0)
val loc2 = Vector(100.0, 200.0, 300.0)

val rectangleArea = loc1.formRectangularPrism(loc2)
val sphereArea = loc1.formSphere(radius = 5.0)
val cylinderArea = loc1.formCylinder(radius = 5.0, height = 10.0)

```

##Combine area operation

You also can use the following function to combine different area

|Function definition|
|----|
|`fun Area.union(target: Area): Area`   |
|`fun Area.intersection(target: Area): Area`   |
|`fun Area.exclusion(target: Area): Area`   |
|`fun Area.difference(target: Area): Area`   |


----
##WorldArea
WorldArea is a wrapper class of Area, it also implemented Area interface with Delegation.
The different of WorldArea and Area is WorldArea is carrying `world` property
, and `contains()` function will check for world property.

In addition, you can get Entities, LivingEntities and Player inside the area by this wrapper.
###Functions & Properties

All Area's function is available for WorldArea. In addition, it have extra properties and functions.


|Function definition|Description|
|----|---|
|`fun toArea(): Area`   | Erase `world` property |


|Properties|Getter|Setter|Description|
|---------------|:----:|:----:|-----------|
|`entities: List<Entity>`| public |  | Get all entities in the area |
|`livingEntities: List<LivingEntity>`| public | | Get all living entities in the area|
|`players: List<Player>`| public | | Get all player in the area|

###Create WorldArea
####From existing Area
```kotlin
val rectangleArea = //...
val rectangleWorldArea = rectangleArea.toWorldArea(worldOf("world"))
```

####From location
```kotlin
val loc1 = locationOf("world", 0.0, 0.0, 0.0)
val loc2 = locationOf("world", 100.0, 200.0, 300.0)

val rectangleWorldArea = loc1.formRectangularPrism(loc2) // with the area creation functions
```
