#Div Element
Full path: `io.reactant.reactant.ui.kits.ReactantUIDivElement`

`div` element is a rectangle container in inventory gui.

##Example
```kotlin
uiService.createUI(player) {
    div {
        margin(1)
        height = 4
        width = 4
        fill(createItemStack(Material.APPLE))
    }
}
```

##Properties & Methods
Inherit from [UIElement](ui-element.md#properties-methods).

|Name|Value|Description|
|:------|-------|-----------|
|`height`|`Int` <br> `MATCH_PARENT` <br> `WRAP_CONTENT` | Height of the element, set Int for absolute value. <br> `MATCH_PARENT` will make it as large as possible to fill the parent. <br> `WRAP_CONTENT` will make it as small as possible to fit the content. |
|`width`|`Int` <br> `MATCH_PARENT` <br> `WRAP_CONTENT` | Width of the element, set Int for absolute value. <br> `MATCH_PARENT` will make it as large as possible to fill the parent. <br> `WRAP_CONTENT` will make it as small as possible to fit the content. |
|`fillPattern`| `(x, y) -> ItemStack`| The background display function of itself. <br> `x` and `y` are relative position in the element. |
|`fill(itemStack)`|`ItemStack`| The shorthand of `fillPattern = (_, _) -> itemStack` <br> The background will fill with the itemStack. |
|`overflowHidden`|`Boolean`|Whether show or hide the overflowing child's item|
|`paddingTop`|`Int`| The top margin between itself and child (inner margin) |
|`paddingRight`|`Int`| The right margin between parent and child (inner margin) |
|`paddingBottom`|`Int`| The bottom margin between parent and child (inner margin) |
|`paddingLeft`|`Int`| The left margin between parent and child (inner margin) |
|`padding`|`List<Int>`| The shorthand of the above padding properties <br> Format: `list(top, right, bottom, left)` |

##Common usage
###Split an inventory into 1:2 view
```kotlin
uiService.createUI(player) {
    div {
        id = "leftCol"
        width = 3
        height = MATCH_PARENT
        display = ElementDisplay.INLINE_BLOCK
        fill(Material.RED_STAINED_GLASS_PANE)
    }
    div {
        id = "rightCol"
        width = 6
        height = MATCH_PARENT
        display = ElementDisplay.INLINE_BLOCK
        fill(Material.BLUE_STAINED_GLASS_PANE)
    }
}
```
