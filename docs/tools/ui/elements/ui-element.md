#UI Element
Full path: `io.reactant.reactant.ui.element.ReactantUIElement`

`ReactantUIElement` is an abstract implementation of UIElement. 
All classes of the elements provided by reactant are subclass of this element. 

##Properties & Methods
|Name|Value|Description|
|------|-------|-----------|
|`display`| `ElementDisplay.BLOCK` <br> `ElementDisplay.INLINE_BLOCK`|All element will be place in new line after a `BLOCK`. <br> `INLINE_BLOCK` allow other element render at the same line.|
|`id`|`String` | Used to identify an element. Should be unique in an inventory.|
|`marginTop`|`Int`| The top margin between parent and itself |
|`marginRight`|`Int`| The right margin between parent and itself |
|`marginBottom`|`Int`| The bottom margin between parent and itself |
|`marginLeft`|`Int`| The left margin between parent and itself |
|`margin`|`List<Int>`| The shorthand of the above margin properties <br> Format: `list(top, right, bottom, left)` |
|`attributes`|`HashMap<String,String>`| Extra data fields for ui developing |
