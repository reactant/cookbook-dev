#Collections
##Tools
<div class='category-container'>
    [<div class='category-btn waves-effect waves-light btn'>
        <i class="fas fa-bolt fa-2x"></i>
        <div>Events</div>
    </div>](events/index.md)
    [<div class='category-btn waves-effect waves-light btn'>
        <i class="fas fa-clock fa-2x"></i>
        <div>Scheduler</div>
    </div>](scheduler/index.md)
    [<div class='category-btn waves-effect waves-light btn'>
        <i class="fas fa-cogs fa-2x"></i>
        <div>Config Loader</div>
    </div>](config/index.md)
    [<div class='category-btn waves-effect waves-light btn'>
        <i class="fas fa-terminal fa-2x"></i>
        <div>Command</div>
    </div>](command/index.md)
    [<div class='category-btn waves-effect waves-light btn'>
        <i class="fas fa-th fa-2x"></i>
        <div>UI</div>
    </div>](ui/index.md)
    <div class='category-btn waves-effect waves-light btn disabled'>
        <i class="fas fa-dollar-sign fa-2x"></i>
        <div>Economy</div>
    </div>
    <div class='category-btn waves-effect waves-light btn disabled'>
        <i class="fas fa-blender fa-2x"></i>
        <div>Resource Stirrer</div>
    </div>
</div>

##Utils
<div class='category-container'>
    [<div class='category-btn waves-effect waves-light btn'>
        <i class="fas fa-map-marker-alt fa-2x"></i>
        <div>Location</div>
    </div>](utils/location/index.md)
    [<div class='category-btn  waves-effect waves-light btn'>
        <i class="fas fa-cube fa-2x"></i>
        <div>ItemStack Builder</div>
    </div>](utils/itemstack/index.md)
    [<div class='category-btn waves-effect waves-light btn'>
        <i class="fas fa-vector-square fa-2x"></i>
        <div>Area</div>
    </div>](utils/area/index.md)
</div>
