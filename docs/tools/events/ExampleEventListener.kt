    @Component
    class ExampleEventListener(
            private val eventService: EventService
    ) : LifeCycleHook {
        override fun onEnable() {
            register(eventService) {
                PlayerJoinEvent::class.observable()
                        .subscribe { it.player.sendMessage("Welcome to our server") }

                PlayerJoinEvent::class.observable()
                        .filter { it.player.level >= 10 }
                        .subscribe { it.player.sendMessage("Only level 10 or above can see this secret message!") }
                // more if any ...
            }
        }
    }
