#Events
Instead of register the event listeners with traditional way,
you can register the event using the service we provided,
which will return a [ReactiveX Observable](http://reactivex.io/documentation/observable.html) object. 
By using the operators of observable, the readability of the flow of event handling will be improved.

##Using Injectables
- io.reactant.reactant.service.spec.server.EventService

##Usage
###Register an event
Since EventService implemented the [Registrable](../../core/registrable.md) interface,
we can register the command with dsl using register() method


!!! note "ExampleEventListener.kt"
    ~~~kotlin
{!tools/events/ExampleEventListener.kt!}
    ~~~

###Register with priority
You can get the observable with different priority
```kotlin
PlayerJoinEvent::class.observable(EventPriority.HIGHEST)
        .subscribe { it.player.sendMessage("Highest priority!") }
```

###Alternative event block
There have an alternative way to register event
```kotlin
PlayerJoinEvent::class{
    filter { it.player.level >= 10 }
            .subscribe { it.player.sendMessage("Oh, another way!") }
    
    filter { it.player.level < 10 }
            .subscribe { it.player.sendMessage("Yes, it is another one") }
}
```
