    @CommandLine.Command(
            name = "bye",
            aliases = ["kick"],
            mixinStandardHelpOptions = true, // auto generate --help option
            description = ["Inverted command of HelloWorld"]
    )
    class ByeWorldCommand : SwCommand() {
        @CommandLine.Parameters(paramLabel = "PLAYER_NAME", description = ["Say bye with the player"])
        var playerName:String = ""

        override fun run() {
            (sender as? Player)?.let {
                it.kickPlayer("Bye :)")
                stdout.out("You kicked ${it.name}")
            } ?: stdout.out("Player not exist")
        }
    }
