    // ...
    class HelloWorldCommand : SwCommand() {
        override fun run() {
            // action when a sender call the command
            stdout.out("Hello world! ${sender.name}");
            showUsage()
        }
    }
