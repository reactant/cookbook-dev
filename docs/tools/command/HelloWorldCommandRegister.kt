    private class HelloWorldCommandRegister(
            val commandService: PicocliCommandService
    ) : LifeCycleHook {
        override fun onEnable() {
            register(commandService) {
                command({ HelloWorldCommand() })
                // or with reference:
                // command(::HelloWorldCommand)
            }
        }
    }
