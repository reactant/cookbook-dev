    private class ReactantCommandRegister(
            val commandService: PicocliCommandService
    ) : LifeCycleHook {
        override fun onEnable() {
            register(commandService) {
                command({ HelloWorldCommand() }){
                    command({ ByeWorldCommand() })
                }
            }
        }
    }
