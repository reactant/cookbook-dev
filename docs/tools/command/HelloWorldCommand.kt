    @CommandLine.Command(
            name = "sayhello",
            aliases = ["hello", "sayhi"],
            mixinStandardHelpOptions = true, // auto generate --help option
            description = ["Just a simple command to say hello"]
    )
    class HelloWorldCommand : SwCommand() {
        override fun run() {
            // action when a sender call the command
            stdout.out("Hello world! ${sender.name}");
        }
    }
