#Command
Reactant command service is implemented using [picocli](https://github.com/remkop/picocli).
This page will only included the basic information of creating a command,
you can find more info about making a command (e.g., Arity, Options, Type Converters) in the [picocli document](https://picocli.info/). 

##Using Injectables
- io.reactant.reactant.extra.command.PicocliCommandService

##Usage
### Top-level command
Firstly, create a simple command, note that you have to extends SwCommand.

!!! note "HelloWorldCommand.kt"
    ~~~kotlin
{!tools/command/HelloWorldCommand.kt!}
    ~~~

Now create a Component and register the command we just created with PicocliCommandService. 
Since PicocliCommandService implemented the [Registrable](../../core/registrable.md) interface,
we can register the command with dsl using register() method

!!! note "HelloWorldCommandRegister.kt"
    ~~~kotlin
{!tools/command/HelloWorldCommandRegister.kt!}
    ~~~
    
The "Hello World! xxx" message should be shown after you type `/sayhello` or `hello` or `sayhi`
    

!!! question "Can I inject injectables inside the command?"
    No, you can't, but you can add parameter in your command constructor,
    and construct command with injectables in the register Component
    
### SubCommand

Sometimes there will have some subcommand under a top-level command,
just like `reactant new` of our cli tools,
we are going to create a subcommand `/sayhello bye PLAYER_NAME` which will kick the player

Firstly, we create a child command.

!!! note "ByeWorldCommand.kt"
    ~~~kotlin
{!tools/command/ByeWorldCommand.kt!}
    ~~~
    
And make HelloWorldCommand show usage when called.

!!! note "HelloWorldCommand.kt"
    ~~~kotlin hl_lines="6"
{!tools/command/HelloWorldCommand_edited.kt!}
    ~~~

Finally, add the subcommand inside register object.

!!! note "HelloWorldCommandRegister.kt"
    ~~~kotlin hl_lines="7"
{!tools/command/HelloWorldCommandRegister_edited.kt!}
    ~~~

!!! danger "Do not register subcommands with annotation attribute"
    In picocli document, you will found that there have a way to register command with 
     `subcommands` attribute inside `@Command`, it is not supported inside reactant.

