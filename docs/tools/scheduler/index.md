#Scheduler
Instead of register the scheduler with traditional way,
we have provided a scheduler service which allow you to schedule with ReactiveX way.

##Using Injectables
 - io.reactant.reactant.service.spec.server.SchedulerService
 
##Functions

|Function definition| Description|
|---|---|
|`fun next(): Completable`                                  |Complete on next tick  |
|`fun timer(delay: Long): Completable`                      |Complete after delayed |
|`fun interval(period: Long): Observable<Int>`              |Observable which will fire counting with period interval|
|`fun interval(delay: Long, period: Long): Observable<Int>` |Observable which will start after delay, and fire counting with period interval|

##Task cancelling
To cancel the task, you can call `depose()` on your disposable.
###Example
```kotlin
val disposable = schedulerService.next().subscribe { sayHi() }
disposable.dispose() // task should be cancelled
```

##ReactiveX Scheduler
If you have tried RxAndroid before, you should know about `AndroidSchedulers.mainThread()`,
Reactant is also providing the similar property named `ReactantCore.mainThreadScheduler` which is implemented using `BukkitScheduler.runTask()`.
With this feature, you are allowed to switch from other thread to server main thread.
###Example
```kotlin
httpService.get("http://web.example")
        .subscribeOn(Schedulers.io())
        .observeOn(ReactantCore.mainThreadScheduler) 
        .subscribe{ debug("Check whether is logging on main thread") }
```
