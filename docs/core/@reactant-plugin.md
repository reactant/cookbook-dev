#@ReactantPlugin

`@ReactantPlugin` is used to tell Reactant where to find your Component class.
Reactant will scan all classes using reflection in your plugin jar, 
and filter by the provided packages path.

The package path should only included your plugin owned classes,
do not use some paths like `net`, `com` or `org`,
or it may cause severe problem if your jar shaded other plugin's class

##Usage
`@ReactantPlugin` must use on the main class of plugin, multiple packages path is available.
```kotlin
@ReactantPlugin(["com.example.plugin.fly", "com.example.plugin.anotherflypath"])
class FlyPlugin : JavaPlugin(){

}
```
