#LifeCycleHook
Implementing LifeCycleHook is not a requirement for a Component, you can just simply declare your Component without implements it if you don't need it.

We have three hooks in the Component LifeCycle, `onEnable()`,`onSave()` and `onDisable()`.


```mermaid
graph TD;
    F-->|Yes|G
    C-->D
    A["Primary constructor"] --> B[Properties injection]
    B -->|Enable| C("onEnable()")
    G("onDisable()")
    
    subgraph Active State
        D["Active"]
        D -->|Save|E("onSave()")
        E-->F{Disabling?}
        F-->|No|D
    end
    
    style C fill:#ffd89b
    style E fill:#ffd89b
    style G fill:#ffd89b
```
