# Manually Project Creation
We suggest to use CLI to create the plugin project. 
If you would like to use other build tools, here is the information you need.

### Create a HelloWorld Component
1. Create a folder called "MyFirstPlugin" and go into the folder.
2. Run command `gradle init`
    1. Select type of project to generate: `basic`
    2. Select build script DSL: `kotlin`
    3. Project name: default
3. Edit `gradle.build.kts`

!!! note "gradle.build.kts"

    ~~~~kotlin
    import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
    import java.net.URI
    
    val kotlinVersion = "1.3.31"
    
    plugins {
        java
        kotlin("jvm") version "1.3.31"
    }
    
    val compileKotlin by tasks.getting(KotlinCompile::class) {
        kotlinOptions.jvmTarget = "1.8"
        kotlinOptions.freeCompilerArgs = listOf("-Xjvm-default=compatibility")
    }
    
    repositories {
        mavenCentral()
        maven { url = URI.create("https://hub.spigotmc.org/nexus/content/repositories/snapshots") }
        maven { url = URI.create("https://dl.bintray.com/reactant/reactant") }
    }
    
    dependencies {
        compileOnly(kotlin("stdlib-jdk8"", kotlinVersion))
        compileOnly("org.spigotmc:spigot-api:1.13.2-R0.1-SNAPSHOT")
        compileOnly("io.reactant:reactant:<version>")
    }
    ~~~~

### Create plugin.yml

1. Create resource folder `src/main/resources`.
2. Create `plugin.yml` just like a normal spigot plugin but depend on Reactant.

!!! note "src/main/resources/plugin.yml"
    ~~~~yaml
    main: com.example.hello.HelloWorld
    name: HelloWorld
    version: '1.0'
    depend:
      - Reactant
    ~~~~
   
### Create Plugin Class

1. Create source folder `src/main/kotlin`.
2. Create package `com.example.helloworld`.
3. Create Kotlin file `HelloWorld.kt`,
    and extends JavaPlugin just like a spigot plugin class,
    but with an annotation [`@ReactantPlugin`](core/@reactant-plugin.md).

!!! note "src/main/kotlin/com/example/helloworld/HelloWorld.kt"
    ~~~~kotlin
    @ReactantPlugin(["com.example.helloworld"])
    class HelloWorld : JavaPlugin()

    ~~~~
    
