$(document).ready(() => {
    renderCodeBlocks()
});

function renderCodeBlocks() {
    const $codeTabContainer = $("[data-code-tabs]").addClass("row z-depth-1 code-tabs");

    $codeTabContainer.each(function () {
        const $codeTabs = $(this).find("[data-code-tab-title]");
        const $codeTabsRow = $("<ul class='tabs'></ul>");
        $codeTabs.each(function () {
            const tmpId = Math.random().toString();
            const blockId = tmpId.substr(2, tmpId.length);
            const blockTitle = $(this).data("code-tab-title");
            $(this).attr("id", blockId).css("min-height", 0).find("p:empty").remove();
            $codeTabsRow.append(`<li class="tab col s3"><a  href="#${blockId}">${blockTitle}</a></li>`)
        });

        $(this).prepend($codeTabsRow);
        $codeTabsRow.tabs();
    });

}


