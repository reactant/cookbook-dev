# Installation
## For new project
Download the CLI, unzip it, and add it into `PATH` in environment variables.

[<button class="waves-effect waves-light btn-small cyan">Get latest CLI</button>](https://gitlab.com/reactant/reactantcli/-/jobs/artifacts/master/raw/build/distributions/reactant-cli.zip?job=build)

Create the project with CLI:
```bash
reactant new <your_project_name>
```

## For existing plugin
Add as dependency:
<div data-code-tabs><div data-code-tab-title="Kotlin DSL">

~~~~kotlin
repositories {
    maven { url = URI.create("https://dl.bintray.com/setako/reactant") }
}

dependencies {
    compileOnly("io.reactant:reactant:<reactant_version>")
}
~~~~
</div><div data-code-tab-title="Groovy DSL">

~~~~groovy
repositories {
    maven {
        url = "https://dl.bintray.com/setako/reactant"
    }
}

dependencies {
    compileOnly "io.reactant:reactant:<reactant_version>"
}
~~~~
</div></div>
    
## Build
If you would like to compile reactant yourself.

<div data-code-tabs><div data-code-tab-title="Linux">

```bash
git clone https://gitlab.com/reactant/reactant.git
cd reactant
./gradlew build
```

</div><div data-code-tab-title="Windows">

```bat
git clone https://gitlab.com/reactant/reactant.git
cd reactant
gradlew build
```

</div></div>
